/*******************************************************************************
File name: utilities.h
Version  : V1.0.0
*******************************************************************************/

/*******************************************************************************

*******************************************************************************/

/*******************************************************************************
                              File description

*******************************************************************************/
/***************************** Include section ********************************/
#ifndef _UTILS_H_
#define _UTILS_H_
#include "Arduino.h"
/**
* @brief Get dealta time in milisecond from now
* @param timeFrom Time from
* @param timeEnd Time end
* @retval bool
*/
bool Utilities_deltaTimeFromNowIsGreater(uint32_t timeFrom, uint32_t timeEnd);
/**
* @brief Get dealta time in milisecond from now
* @param timeFrom Time from
* @retval bool
*/
uint32_t Utilities_getdeltaTimeFromNow(uint32_t timeFrom);
/**
* @brief Get current time stamp
* @retval uint32
*/
uint32_t Utilities_getCurrentTime(void);
/**
* @brief Get the largest number in array
* @param data Data array
* @param length Length of this array
* @retval int16_t
*/
int16_t Utilities_largest(int16_t *data, size_t length);
/**
* @brief Get the average number in array
* @param data Data array
* @param length Length of this array
* @retval int16_t
*/
int16_t Utilities_getAverage(int16_t *data, size_t length);
/**
* @brief Make a delay in miliseconds
* @param timeout Time in miliseconds to delay
* @retval bool
*/
bool Utilities_makeDelays(uint32_t timeout);
/**
* @brief Get time and date
* @retval tm
*/
struct tm* Utilities_getTimeAndDate(void);
/**
* @brief Period time of
* @param time Time variable in milisecond
* @param period Expected period in milisecond
* @retval bool
*/
bool Utilities_periodTimeOf(uint32_t *time, uint32_t period);
#endif
/*******************************************************************************
                                     END OF FILE
*******************************************************************************/