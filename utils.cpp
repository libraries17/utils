/*******************************************************************************
File name: utilities.cpp
Version  : V1.0.0
*******************************************************************************/

/*******************************************************************************

*******************************************************************************/

/*******************************************************************************
                              File description

*******************************************************************************/
/***************************** Include section ********************************/
#include "utils.h"

bool Utilities_deltaTimeFromNowIsGreater(uint32_t timeFrom, uint32_t timeEnd)
{
    if(labs(millis() - timeFrom) >= timeEnd)
        return true;
    return false;
}

uint32_t Utilities_getdeltaTimeFromNow(uint32_t timeFrom)
{
    return (millis() - timeFrom);
}

uint32_t Utilities_getCurrentTime(void)
{
    return millis();
}

int16_t Utilities_largest(int16_t *data, size_t length)
{
    // Initialize maximum element
    int16_t max = data[0];
    // Traverse array elements from second and
    // compare every element with current max 
    for (size_t i = 1; i < length; i++)
    {
        if (data[i] > max)
            max = data[i];
    }
    return max;
}

int16_t Utilities_getAverage(int16_t *data, size_t length)
{
    uint32_t val = 0;
    for(size_t i = 0; i < length; i++)
    {
        val += data[i];
    }
    val = val / length;
    return val;
}

bool Utilities_makeDelayAsync(uint32_t *name, uint32_t timeout)
{
    if(*name > 0)
    {
        if(millis() - (*name) >= timeout)
        {
            *name = 0;
            return true;
        }
    }
    return false;
}

bool Utilities_makeDelays(uint32_t timeout)
{
    static uint32_t fncCall = 0;
    static uint32_t timeCheck = 0;
    if(fncCall == 0)
    {
        timeCheck = millis();
    }
    else
    {
        if(millis() - timeCheck >= timeout)
        {
            fncCall = 0;
            return true;
        }
    }
    fncCall++;
    return false;
}

bool Utilities_periodTimeOf(uint32_t *time, uint32_t period)
{
    if(millis() - (*time) >= period)
    {
        *time = millis();
        return true;
    }
    return false;
}

struct tm* Utilities_getTimeAndDate(void)
{
    unsigned long milliseconds = millis();
    time_t seconds = (time_t)(milliseconds/1000);
    return localtime(&seconds);
}
/*******************************************************************************
                                     END OF FILE
*******************************************************************************/